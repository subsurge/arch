#!/bin/bash

# Arch install script created by Surge. Assumptions are: keymap dvorak, US English, Mountain time zone, installing single partition to primary hard disk /dev/sda. No wireless. If you need wireless, install dialog and wpa_supplicant and change the DHCP settings later.

# Use a better font, because Linux users accept no defaults
setfont Lat2-Terminus16

echo "NOW YOU'RE INSTALLING ARCH WITH"
echo"   /"
echo"  /"
echo" /____      ___  ___  ___"
echo"     / |  | |  | |  | |__"
echo"    /  |__| |    |__| |__"
echo"   /             ___|"

echo "I'm sorry..."

# NTP FTW
timedatectl set-ntp true

# Make that sweet MBR partition table
echo 'type=83' | sudo sfdisk /dev/sda

# Now lay a filesystem down in that cake pan
mkfs.ext4 /dev/sda1

# Get on that horsie...cake...[analogyfallingapart]
mount /dev/sda1 /mnt

# Install all the things
pacstrap /mnt base linux linux-firmware grub dhcpcd nano sudo

# I don't know what this does, but if you don't do it, nothing works.
genfstab -U /mnt >> /mnt/etc/fstab

# Now climb into that soft, moist root environment and tickle it from the inside! OOOOHHH!!
arch-chroot /mnt

# You made a kid, now you have to name it
read -p "Ok, what's the hostname for this mess? " name
echo $name > /etc/hostname

# You're in Denver, shut up
ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime

# This keeps time from being not time
hwclock --systohc

# Customizations, because Linux
echo KEYMAP=dvorak > /etc/vconsole.conf
echo FONT=Lat2-Terminus16 > /etc/vconsole.conf

echo "Ok, now you gotta do some shit. In a new tty, uncomment your locale in /etc/locale.gen - and while you're at it, go ahead and add keymap and consolefont to the hooks array in /etc/mkinitcpio.conf because I still don't know what cluster of sorcery makes these damn changes permanent."

read -p "Press [Enter] key when you done wit dat."

# The greatest gen
locale-gen

# Make the initial C-3PO
mkinitcpio -p linux

# Make some grub
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg

read -p "Do me a favor. Grab the ethernet interface name for me, would ya? KTHXBAI " int

# Enable the Damn Hairy Cats Producing Canine Deaths
systemctl enable dhcpcd@$int

# Secret sauce time
echo "Sorry to keep bugging you. Do me a favor and enter the root password now."
passwd

# Get the hell outta Dodge
exit

# Let's just...snip this wire here...
umount -R /mnt

echo "Done! Now take out the disk, virtual or otherwise. Let's see how broke we made it."
read -p "Press [Enter] key to reboot..."
reboot